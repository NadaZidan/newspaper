import { Component, OnInit } from '@angular/core';
import { NavService } from '../nav.service';

@Component({
  selector: 'app-general',
  templateUrl: './general.component.html',
  styleUrls: ['./general.component.scss']
})
export class GeneralComponent implements OnInit {

  get newsContainer() {
    return this._navService.newsContainer;
  }
  get firstNews() {
    return this._navService.firstNews;
  }
  get topNews() {
    return this._navService.topNews;
  }
  constructor(private _navService : NavService) {  }


  ngOnInit() {
  }

}
