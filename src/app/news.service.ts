import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NewsService {


  constructor(private _Http:HttpClient) { }
  getNews(countryCode,categoryChoosen):Observable<any>
  {
    return this._Http.get("https://newsapi.org/v2/top-headlines?country="+countryCode+"&category="+categoryChoosen+"&apiKey=a6ca66748b8145e5ae4e8da079d23f0f")
     
  }
}
