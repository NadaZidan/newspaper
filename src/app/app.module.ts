import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';

import {FormsModule, ReactiveFormsModule} from '@angular/forms'
import {HttpClientModule} from '@angular/common/http'
import { GeneralComponent } from './general/general.component';
import { NewsService } from './news.service';
import { NavService } from './nav.service';
import { CommonModule } from '@angular/common';
import { SeemorePipe } from './seemore.pipe';
import { FooterComponent } from './footer/footer.component';
import { CarouselModule } from 'ngx-bootstrap/carousel';

@NgModule({
  declarations: [
    AppComponent,
  
    NavbarComponent,
    GeneralComponent,
    SeemorePipe,
    FooterComponent,
    
    
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ReactiveFormsModule,FormsModule,CommonModule, CarouselModule.forRoot(), 
    
  ],
  providers: [NewsService, NavService],
  bootstrap: [AppComponent]
})
export class AppModule { }
