import { Component, OnInit } from '@angular/core';
import { NavService } from '../nav.service';
import { NewsService } from '../news.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  get countries() {
    return this._navService.countries;
  }
  get categories() {
    return this._navService.categories;
  }
  get newsContainer() {
    return this._navService.newsContainer;
  }
  get changeCode() {
    return this._navService.changeCode;
  }
  get changeCategory() {
    return this._navService.changeCategory;
  }
  get getNews() {
    return this._newsService.getNews;
  }
  

  myDate:any = '';

  getDate()
  {
    setInterval(() => {this.myDate = new Date();}, 1000);
  }



  
  





  constructor(public _navService : NavService , public _newsService : NewsService) {
    this.getDate();

    
    

   }
 
    
    
  
    
       

  ngOnInit() {
    
    

  }

 

}
